<%@ page import="io.muic.ooc.webapp.User" %>
<%@ page import="io.muic.ooc.webapp.MySqlCon" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>

<head>
    <style>
        body {
            background-color: lightblue;
        }
    </style>
</head>
    <body>

    <h2>Edit Profile</h2>

    <%
        if (request.getParameter("status")!=null){
            if (request.getParameter("status").equals("invalidemail")){
            %> <p>This email is already exist</p> <%
            }

        }
    %>

    <form action="/edit" method="post">
        <table border="1" cellpadding="2">

            <%
                String editUser = request.getParameter("editUser");
                System.out.println("JSP: " + editUser);
                User user = MySqlCon.getUser(editUser);
            %>

            <tr>
                <td>
                    Field
                </td>

                <td>
                    Information
                </td>

                <td>
                    Edit Field
                </td>
            </tr>


            <tr>
                <td>
                    Username
                </td>

                <td>
                    <%=user.getUsername()%>
                </td>

                <td>
                    Unavailable
                    <input type="hidden" name="username" value="<%=user.getUsername()%>" >
                </td>

            </tr>

            <tr>
                <td>
                    FirstName
                </td>

                <td>
                    <%=user.getFirstname()%>
                </td>

                <td>
                    New FirstName<br>
                    <input type="text" name="new_fname" placeholder="firstname"/>
                </td>

            </tr>

            <tr>
                <td>
                    LastName
                </td>

                <td>
                    <%=user.getLastname()%>
                </td>

                <td>
                    New LastName: <br>
                    <input type="text" name="new_lname" placeholder="lastname"   />
                </td>
            </tr>

            <tr>
                <td>
                    Email
                </td>

                <td>
                    <%=user.getEmail()%>
                </td>

                <td>
                    New Email:<br>
                    <input type="email" name = "new_email" placeholder="email" />
                </td>
            </tr>

            <tr>
                <td>
                    Password
                </td>

                <td>
                    Hidden
                </td>
                <td>
                    New Password:<br>
                    <input type="password" name="new_password" placeholder="password" /><br>
                </td>
            </tr>

        </table>
        <input type="submit" value="Submit">

    </form>


    <form action="/user" method="GET">
        <input type="submit" value="Cancel">
    </form>

    </body>
</html>