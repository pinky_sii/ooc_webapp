<html>

<head>
    <style>
        body {
            background-color: lightblue;
        }

        h1 {
            color: white;
            /*text-align: center;*/
        }

        p {
            font-family: verdana;
            font-size: 20px;
        }

        form {
            /*text-align: center;*/
        }
    </style>
</head>

    <body>

        <h1>Login</h1>

        <p>${error}</p>

        <form action="/login" method="post">

            Username:<br/>
            <input type="text" name="username"/>
            <br/>

            Password:<br/>
            <input type="password" name="password">
            <br><br>

            <input type="submit" value="Log In">
        </form>
        <form action="/register" method="get">
            <input type="submit" value="Create Account">
        </form>

    </body>
</html>
